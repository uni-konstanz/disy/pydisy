"""Design Patterns

Implements certain OO-design patterns for python such as
abstract base classes, interfaces, singletons and typed data structures.
"""

from abstract import *
from singleton import *
from typed import *
from compress import *