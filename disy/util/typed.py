# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Typed data structures

Implements typed data structures such as typed lists and typed dicts. The
implementation is heavily based on [1].

[1] Hans Petter Langtangen, "Python Scripting for Computational Science",
    TEXTS IN COMPUTATIONAL SCIENCE AND ENGINEERING,
    Volume 3, 2008, DOI: 10.1007/978-3-540-73916-6
"""
__all__ = ['typedlist', 'typeddict']

class typedlist (list):
	"""Typed List
	
	Implements a typed list, a list that must contain elements of a certain
	type. Works exactly as list() with the exception that the class of the first
	item that is inserted is used as the lists type. Alternatively, the list
	type can also be passed to the constructor by setting argument 't' to
	any class (eg int). All subsequent items must be of the same type.
	
	Members:
	__itemclass 	-- 	class of the items
	"""
	def __init__ (self, l=[], t=None):
		"""__init__ ([],type) -- instantiate typedlist object
		
		l 	-- 	list to instantiate, is type checked against t
						or l[0] if t==None
		t 	-- 	a reference class (eg int)
		"""	
		if t.__class__ != None.__class__: self.__itemclass = t
		list.__init__(self,l)
		for item in self: self._check_(item)

	def _check_ (self,item):
		"""_check_(item) -- check type of item
		
		item 	-- 	item to check the type of
		raise 	-- 	TypeError if not isinstance(item,type(self[0]))
		"""
		try:
			if not isinstance(item,self.__itemclass):
				raise TypeError, 'items must be %s not %s' % (self.__itemclass,item.__class__)
		except AttributeError:
			self.__itemclass = item.__class__
		
	def __str__ (self):
		# list.__str__ -> self.__repr__
		return list.__repr__(self)

	def __repr__ (self):
		if hasattr(self,'__itemclass'):
			return "%s(%s,%s)" % \
				(self.__class__.__name__,list.__repr__(self),self.__itemclass.__name__)
		else:
			return "%s(%s)" % (self.__class__.__name__,list.__repr__(self))
	
	def __setitem__ (self,i,item):
		self._check_(item); list.__setitem__(self,i,item)

	def __add__ (self,other):
		return list.__add__(self,other)

	def __iadd__ (self,other):
		return list.__iadd__(self,other)
	
	def __setslice__ (self,s,l):
		for i in l: self._check_(i)
		list.__setslice__(self,s,l)

	def insert (self,index,item):
		self._check_(item); list.insert(self,index,item)

	def append (self,item):
		self._check_(item); list.append(self,item)

	def extend (self,l):
		for i in l: self._check_(i)
		list.extend(self,l)
# typedlist

class typeddict (dict):
	"""Typed Dictionary class
	
	Implements a typed dictionary. Keys and values must be of a specific type.
	Types can be passed as arguments to the constructor. If no types are passed,
	the types of the first {key : value} pair inserted will be used as type
	definitions.
	
	Members:
	__keyclass 	-- 	class of keys
	__valclass 	-- 	class of values
	"""
	def __init__ (self, d = {}, kc = None, vc = None):
		"""__init__ ([],type,type) -- instantiate typedlist object
		"""
		if kc.__class__ != None.__class__: self.__keyclass = kc
		if vc.__class__ != None.__class__: self.__valclass = vc
		dict.__init__(self,d)
		for k in self.keys(): self._check_(k,self[k])

	def __str__ (self):
		# dict.__str__ -> self.__repr__
		return dict.__repr__(self)
	
	def __repr__ (self):
		return "%s(%s)" % (self.__class__.__name__,dict.__repr__(self))

	def _check_ (self,k,v):
		"""_check_(k,v) -- check type of key and val

		raise 	--	TypeError if not isinstance(item,type(self[0]))
		"""
		try: self.__keyclass
		except: self.__keyclass = k.__class__
		try: self.__valclass
		except: self.__valclass = v.__class__
		if not isinstance(k,self.__keyclass) or not isinstance(v,self.__valclass):
			raise TypeError, 'items must be {%s:%s} not {%s:%s}' % \
				(self.__keyclass,self.__valclass,k.__class__,v.__class__)
	
	def __setitem__ (self,k,v):
		self._check_(k,v); dict.__setitem__(self,k,v)
	
	def update (self,E={},**F):
		"""D.update(E, **F) -> None."""
		try:
			E.has_key
			for k in E: self._check_(k,E[k])
		except AttributeError:
			for (k,v) in E: self._check_(k,v)
		for k in F: self._check_(k,F[k])
		return dict.update(self,E,**F)
# typeddict
