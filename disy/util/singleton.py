# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

__all__ = ['singleton']

from abstract import *

class singleton (abstract):
	"""Singleton design pattern
	
	Implements the singleton design pattern. Singletons restrict instantiation
	to exactly one single object. This is achieved by checking if an instance
	is already in existance, if so, the existing object is returned by the
	constructor. Classtypes are compared to ensure that subclasses will create their own
	instance objects.
	
	To create singletons just define classes with (singleton) as base class.
	This guarantees that only one instance can be generated.
	
	Members:
	__instance 	-- 	the instance of the singleton object
	"""
	__instance = None

	def __new__ (cls, *args, **kwargs):
		if cls != type(cls.__instance):
			cls.__instance = object.__new__(cls, *args, **kwargs)
		return cls.__instance
# singleton
