# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""Abstract
Implements an abstract base class. To define abstract classes just inherit
from 'abstract'. An interface can be implemented by using 'abstract' as base
class and define abstract methods calling implement().
"""
__all__ = ['implement', 'abstract']

def implement():
	"""implement() -- raise NotImplementedError"""
	raise NotImplementedError("Method not implemented")

class abstract (object):
	"""Abstract Base Class

	Implements and Abstract Base Class. Abstracts cannot be instantiated.
	To implement abstract classes and interfaces use (abstract)
	as base class and implement interface methods that call the function
	implement().
	"""
	def __init__ (self):
		"""__init__() -- raises exception"""
		implement()

	def __repr__ (self):
		"""__repr__ (self) -> return canonical string representation"""
		return "<%s.%s instance at %s>" % \
			(self.__class__.__module__,self.__class__.__name__,hex(id(self)))

	def __str__ (self):
		"""__str__ (self) -> return string representation"""
		return str(self.__dict__)
# abstract
