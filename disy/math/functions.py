# -*- coding: utf-8 -*-
###
# Copyright (c) 2012-2017 thomas.zink _at_ uni-konstanz _dot_ de
#
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

"""functions.py

Implements some useful math and statistics functions which are missing in
pythons standard library.
"""

from operator import *

def mean(x):
	"""mean(x) -> computes the mean / average"""
	return float(sum(x))/len(x)

def var(x):
	"""var(x) -> computes the variance"""
	m = mean(x)
	return sum([(i-m)**2 for i in x])/(len(x)-1)
	
def stdev(x):
	"""stdev(x) -> computes the standard deviation"""
	return sqrt(var(x))

def fact (x):
	"""fact(x) -> x!, computes the factorial"""
	return 1 if x <= 1 else reduce(mul, xrange(2,x+1))

def perm (n,r):
	"""perm(n,r) -> compute permutations of drawing r from set n"""
	if r<=1:
		if r == 1: return n
		if r == 0: return 1
		if r <= 0: return 0
	return reduce(mul, xrange(n-r+1,n+1))

def comb (n,r):
	"""comb(n,r) -> compute combinations of drawing r from set n"""
	return div(perm(n,r),fact(r))

def hist (items):
	"""hist([items]) -> { item : frequency }

	Calculates a histogram of the frequency of items
	in the sequence [items].
	"""
	h = {}
	for i in items:
		try: h[i] += 1
		except (KeyError): h[i] = 1
	return h
